package it.unibo.oop.lab.collections1;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Example class using {@link List} and {@link Map}.
 * 
 */
public final class UseCollection {

	private static final int RANGE= 1000;
	
	private static final int ELEMS = 100000;
	private static final int TO_MS = 1000000;
	
	private static final int ITERATION_TIMES = 1000;

	public static double insertElementTimePrint(List<Integer> list, int elem) {
		long time = System.nanoTime();
		/*
		 * Run the benchmark
		 */
		for (int i = 1; i <= ELEMS; i++) {
			list.add(i);
		}
		/*
		 * Compute the time and print result
		 */
		time = System.nanoTime() - time;

		System.out.println("Inserting " + ELEMS + " element in " + list.getClass().getName() + " took " + time + "ns ("
				+ time / TO_MS + "ms)");
		
		return time;
	}
	
	public static double readElementTimePrint(List<Integer> list, int position, int readingTimes) {
		long time = System.nanoTime();
		/*
		 * Run the benchmark
		 */
		list.get(position);
		/*
		 * Compute the time and print result
		 */
		time = System.nanoTime() - time;

		System.out.println("Reading "+readingTimes+ " times the element in position "+position+  " took " + time + "ns ("
				+ time / TO_MS + "ms) in class"+ list.getClass().getName());
		
		return time;
	}

	private UseCollection() {

	}

	/**
	 * @param s
	 *            unused
	 */
	public static void main(final String... s) {
		/*
		 * 1) Create a new ArrayList<Integer>, and populate it with the numbers from
		 * 1000 (included) to 2000 (excluded).
		 */

		List<Integer> integerArrayList = new ArrayList<Integer>();

		for (int i = RANGE; i < RANGE+RANGE; i++) {
			integerArrayList.add(i);
		}

		/*
		 * 2) Create a new LinkedList<Integer> and, in a single line of code without
		 * using any looping construct (for, while), populate it with the same contents
		 * of the list of point 1.
		 */

		List<Integer> integerLinkedList = new LinkedList<Integer>(integerArrayList);

		/*
		 * 3) Using "set" and "get" and "size" methods, swap the first and last element
		 * of the first list. You can not use any "magic number". (Suggestion: use a
		 * temporary variable)
		 */

		Integer temp = integerArrayList.get(0);
		integerArrayList.set(0, integerArrayList.get(integerArrayList.size() - 1));
		integerArrayList.set(integerArrayList.size() - 1, temp);

		/*
		 * 4) Using a single for-each, print the contents of the arraylist.
		 */

		for (Integer n : integerArrayList) {
			System.out.println(n);
		}

		/*
		 * 5) Measure the performance of inserting new elements in the head of the
		 * collection: measure the time required to add 100.000 elements as first
		 * element of the collection for both ArrayList and LinkedList, using the
		 * previous lists. In order to measure times, use as example
		 * TestPerformance.java.
		 */

		insertElementTimePrint(integerArrayList, ELEMS);
		insertElementTimePrint(integerLinkedList, ELEMS);

		/*
		 * 6) Measure the performance of reading 1000 times an element whose position is
		 * in the middle of the collection for both ArrayList and LinkedList, using the
		 * collections of point 5. In order to measure times, use as example
		 * TestPerformance.java.
		 */
		
		readElementTimePrint(integerArrayList, integerArrayList.size()/2, ITERATION_TIMES);
		readElementTimePrint(integerLinkedList, integerLinkedList.size()/2, ITERATION_TIMES);

		/*
		 * 7) Build a new Map that associates to each continent's name its population:
		 * 
		 * Africa -> 1,110,635,000
		 * 
		 * Americas -> 972,005,000
		 * 
		 * Antarctica -> 0
		 * 
		 * Asia -> 4,298,723,000
		 * 
		 * Europe -> 742,452,000
		 * 
		 * Oceania -> 38,304,000
		 */
		
		
		Map<String,Long > world = new TreeMap<String, Long>();
		world.put("Africa", 1_110_635_000L);
		world.put("Americas", 972_005_000L);
		world.put("Antarctica", 0L);
		world.put("Asia", 4_298_723_000L);
		world.put("Oceania", 38_304_000L);
		

		/*
		 * 8) Compute the population of the world
		 */
		long worldPopulation= 0;
		for ( Long n : world.values()) {
			worldPopulation = worldPopulation+n; 
		}
		
		System.out.println("The world population is of "+worldPopulation+ " people ");
		
	}
}
